# -*- coding: utf-8 -*-
import base64
import io

from odoo import models

class StudentCardXlsx(models.AbstractModel):
    _name = 'report.school_management.report_bs_student_template_xls'
    _inherit = 'report.report_xlsx.abstract'

    def generate_xlsx_report(self, workbook, data, students):
        print(":::::::students", students)
        sheet = workbook.add_worksheet('Student Card')
        bold = workbook.add_format({'bold': True})
        format_1 = workbook.add_format({'bold': True, 'align': 'center', 'bg_color': 'yellow'})

        row = 1
        col = 1
        sheet.set_column('B:B', 15)
        sheet.set_column('C:C', 15)
        for obj in students:
            row += 1
            sheet.merge_range(row, col, row, col + 1, ' ID Card', format_1)

            row += 1
            if obj.profile_img:
                student_img = io.BytesIO(base64.b64decode(obj.profile_img))
                sheet.insert_image(row, col, "profile_img.png", {'image_data': student_img, 'x_scale': 0.5, 'y_scale':0.5 })
                row += 5
            # row += 1
            sheet.write(row, col, 'Student Id', bold)
            sheet.write(row, col + 1, obj.student_id)
            row += 1
            sheet.write(row, col, 'Name', bold)
            sheet.write(row, col + 1, obj.last_name)
            row += 1
            sheet.write(row, col, 'Status', bold)
            sheet.write(row, col + 1, obj.status)

            row += 2




