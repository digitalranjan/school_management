from odoo import models, fields, api
from io import BytesIO
import xlsxwriter

# from odoo_16.odoo.http import route


class ExportSaleOrderWizard(models.TransientModel):
    _name = 'my_module.export_sale_order_wizard'

    user_id = fields.Many2one('res.users', string='User')
    sale_order_ids = fields.Many2many('sale.order', string='Sale Orders')

    @api.onchange('user_id')
    def onchange_user_id(self):
        # Get the sale orders for the selected user
        sale_orders = self.env['sale.order'].search([('user_id', '=', self.user_id.id)])
        self.sale_order_ids = sale_orders

    @api
    def export_data(self):
        # Get the data to export
        data = self.sale_order_ids

        # Create an Excel workbook
        output = BytesIO()
        workbook = xlsxwriter.Workbook(output, {'in_memory': True})
        worksheet = workbook.add_worksheet()

        # Write the headers
        headers = ['Order Number', 'Customer', 'Order Date', 'Total Amount']
        for i, header in enumerate(headers):
            worksheet.write(0, i, header)

        # Write the data rows
        for i, record in enumerate(data):
            worksheet.write(i+1, 0, record.name)
            worksheet.write(i+1, 1, record.partner_id.name)
            worksheet.write(i+1, 2, record.date_order)
            worksheet.write(i+1, 3, record.amount_total)

        # Close the workbook
        workbook.close()

        # Return the Excel file
        output.seek(0)
        return self.env['ir.actions.act_url'].with_context(file_data=output.read(), file_name='export.xlsx').\
            tag_('a', href=route, download=self.file_name)
