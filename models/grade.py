# -*- coding: utf-8 -*-

from odoo import fields, models, api

class Grade(models.Model):
    """School Grade Object"""
    _name = 'bs.grade'
    _rec_name = 'name'
    _description = "Academic Grade"

    name = fields.Char(string='Name', required=True)
    email = fields.Char(string='Email')
    class_ids = fields.One2many('bs.class', 'grade_id', string='Class') # 'bs.class'

    progress_percentpie = fields.Integer(compute='_compute_progress_percentpie')

    @api.depends('name', 'email')
    def _compute_progress_percentpie(self):
        for rec in self:
            if rec.name and rec.email:
                progress = 100
            elif rec.name:
                progress = 50
            else:
                progress = 0
            rec.progress_percentpie = progress

    # Button Action
    def action_new(self):
        print("I am  called form New Application")
        pass

    def action_done(self):
        print("I am  called form Done Application")
        pass

    def action_reject(self):
        print("I am  called form Reject Application")
        pass