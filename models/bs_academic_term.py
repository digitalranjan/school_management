# -*- coding: utf-8 -*-

from odoo import fields,  models

class BsAcademicTerm(models.Model):
    """Academic Term Object"""
    _name = 'bs.academic.term'
    _rec_name = 'name'
    _description = "Academic Term Details"

    name = fields.Char(string='Name', required=True)
    # academic_term_id = fields.Many2one('bs.academic.year', string='Academic Term')
    start_date = fields.Date(string='Start Date', required=True)
    end_date = fields.Date(string='End date', required=True)
    student_id = fields.Many2one('bs.student', string='Student')


    academic_year_id = fields.Many2one('bs.academic.year', string='Academic Year')

