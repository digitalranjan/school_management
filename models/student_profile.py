# -*- coding: utf-8 -*-

from odoo import fields,  models, api, _
from dateutil.relativedelta import relativedelta
from datetime import datetime, date
# from odoo.exceptions import  ValidationError


class StudentProfile(models.Model):
    """Student Profile Object"""
    _name = 'bs.student'
    _rec_name = 'first_name'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _description = "Student Profile"

    full_name = fields.Char(string='Name', compute='_compute_name')
    student_id = fields.Char(string='Student ID', required=True, copy=False, readonly=True, default=lambda self: _('New'))
    first_name = fields.Char(string='First Name', required=True)
    middle_name = fields.Char(string='Middle Name')
    last_name = fields.Char(string='Last Name', required=True)
    date_of_birth = fields.Date(string='Date Of Birth', required=True)
    age = fields.Char(string='Age') # _compute='_compute_age'
    gender = fields.Selection([('male', 'Male'), ('female', 'Female')], string='Gender')
    current_grade_id = fields.Many2one('bs.grade', string='Current Grade')
    profile_img = fields.Image(attachment=True)
    address = fields.Text(string='Address')
    phone = fields.Char(string='Phone')
    email = fields.Char(string='Email')
    study = fields.Char(string='Field of Study')
    school = fields.Char(string='School')
    # department = fields.Many2one('academic.year', string='Department')
    manager = fields.Many2one('res.partner', string='Manager', tracking=True)
    school_teacher = fields.Many2many('school.teacher', string='School Teacher', tracking=True) #kanban in page
    course = fields.Many2one('bs.course', string='Course')
    my_url = fields.Char(string='My Url')
    my_phone = fields.Char(string='Phone')
    certificate = fields.Selection([('bachelore', 'Bachelore'),
                                        ('master', 'Master'),
                                        ('other', 'Other')], string='Certificate Level', tracking=True)
    
    status = fields.Selection([('new', 'New'),
                               ('training', 'Training'),
                               ('employeed', 'Employeed'),
                               ('rejected', 'Rejected'), ], default='new', tracking=True)

    priority = fields.Selection([('clear', 'Clear'),
                                ('urgent', 'Urgent'),
                                ('normal', 'Normal'),
                                ('lowand', 'Lowand'),
                                  ('high', 'High')], copy=False, default='normal', required=True)

    # date_begin = fields.Datetime(string='Start Date') # date counter
    address = fields.Text(string='Address')
    # address fields
    street = fields.Char()
    street2 = fields.Char()
    zip = fields.Char(change_default=True)
    city = fields.Char()
    state_id = fields.Many2one("res.country.state", string='State', ondelete='restrict',
                               domain="[('country_id', '=?', country_id)]")
    country_id = fields.Many2one('res.country', string='Country', ondelete='restrict')
    country_code = fields.Char(related='country_id.code', string="Country Code")
    def action_training(self):
        if self.status == 'new':
            self.status = 'training'

    def action_employeed(self):
        if self.status == 'training':
            self.status = 'employeed'

    def action_new(self):
        self.status = 'new'

    def action_rejected(self):
        self.status = 'rejected'
    # current_class_id = fields.Many2one('bs.class', string='Current Class')
    # family_id = fields.Many2one('', string='Family ID') # some function ....

    #first name + Last name  compute
    @api.depends('first_name', 'last_name')
    def _compute_name(self):
        for rec in self:
            rec.full_name = '%s - %s' % (rec.first_name or '', rec.last_name or '')

    #Student Id Sequnce
    @api.model
    def create(self, vals):
        if vals.get('student_id', _('New')) == _('New'):
            vals['student_id'] = self.env['ir.sequence'].next_by_code('bs.student') or _('New')
        return super().create(vals)

    #age calculate onchange
    @api.onchange('date_of_birth')
    def set_age(self):
        print("age", self)
        for rec in self:
            if rec.date_of_birth:
                user_dob = rec.date_of_birth
                print("date of Birth form  user", user_dob)
                birth_date = datetime.strptime(str(user_dob), "%Y-%m-%d")
                today_date = date.today()
                result = relativedelta(today_date, birth_date)
                rec.age = str(result.years) + ' years'

    # # #future date Constrain.of DOB.
    # @api.constrains('date_of_birth')
    # def _check_future_date(self):
    #     if self.date_of_birth <= datetime.today():
    #         raise ValidationError('Expiration date must be after today.')





