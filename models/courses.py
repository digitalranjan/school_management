# -*- coding: utf-8 -*-

from odoo import fields,  models

class Courses(models.Model):
    """School Courses Object"""
    _name = 'bs.course'
    _rec_name = 'code'
    _description = "Courses Details"

    name = fields.Char(string='Name', required=True)
    code = fields.Char(string='Code', required=True)
    # parent_course = fields.Many2one(string='Parent Course') # bs.course
    evalution_type = fields.Selection([('normal', 'Normal'),
                                       ('cwa', 'CWA'),
                                       ('gpa', 'GPA'),
                                       ('cce', 'CCE')], string='Evalution Type')
