from . import bs_academic_year
from . import bs_academic_term
from . import grade
from . import bs_class
from . import family
from . import student_profile
from . import parent_profile
from . import courses
from . import subject
from . import school_teacher

