# -*- coding: utf-8 -*-

from odoo import fields,  models, api , _
from odoo.exceptions import  ValidationError

class ParentProfile(models.Model):
    """Parent Profile Object"""
    _name = 'bs.parent'
    _description = "Parent Profile"

    name = fields.Char(string='Name', required=True)
    address_1 = fields.Char(string='Address1')
    address_2 = fields.Char(string='Address2')
    phone = fields.Char(string='Phone')
    email = fields.Char(string='Email')
    family_id = fields.Many2one('bs.family', string='Family ID')

    @api.onchange('phone')
    def onchange_phone_no(self):
        print("###Onchange Phone", self.phone)
        # for record in self:
        if self.phone:
            if len(self.phone):
                ref = self.phone
                ref = '-'.join([ref[:3], ref[3:6], ref[6:8], ref[8:]])
                self.phone = ref

    # @api.multi
    @api.constrains('phone')
    def _check_phone_number(self):
        for rec in self:
            if rec.phone and len(rec.phone) != 13:
                raise ValidationError(_("Phone no Must be 10 Digit... and - are 3"))
        return True