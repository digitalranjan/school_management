# -*- coding: utf-8 -*-

from odoo import fields,  models

class Subjects(models.Model):
    """School Subjects Object"""
    _name = 'bs.subjects'
    _rec_name = 'name'
    _description = "Courses Details"

    name = fields.Char(string='Name', required=True)
    code = fields.Char(string='Code', required=True)
    type = fields.Selection([('theory', 'Theory'),
                             ('practical', 'Practical'),
                             ('both', 'Both'),
                             ('other', 'Other')],
                             string='Type', required=True)
    subject_type = fields.Selection([('compulsory', 'Compulsory'),
                                     ('elective', 'Elective')], string='Subject Type')
    grade_weightage = fields.Float(string='Grade Weightage')