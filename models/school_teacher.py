# -*- coding: utf-8 -*-

from odoo import fields,  models, api, _
from datetime import datetime, date
from odoo.exceptions import ValidationError

class SchoolTeacher(models.Model):
    ''' Defining a Teacher information '''
    _name = 'school.teacher'
    _description = 'Teacher Information'

    name = fields.Char(string='Name', required=True)
    email = fields.Char(string='Email') # required=True
    date_of_birt = fields.Date(string='Date Of Birth')
    employee_id = fields.Many2one('hr.employee', string='Employee ID', required=True)
    standard_id = fields.Many2one('bs.grade', string='Academic Class')
    stand_ids = fields.Many2many('bs.class', string='Assigned to teacher')
    subject_id = fields.Many2one('bs.subjects', string='Course-Subjects')
    profile_img = fields.Image(attachment=True)
    priority = fields.Selection([('clear', 'Clear'),
                                 ('urgent', 'Urgent'),
                                 ('normal', 'Normal'),
                                 ('lowand', 'Lowand'),
                                 ('high', 'High')], copy=False, default='normal', required=True)
    login_time = fields.Datetime(string='Check In')
    logout_time = fields.Datetime(string='Check Out')
    hours = fields.Float(compute='_compute_hours', store=True)

    @api.depends('login_time', 'logout_time')
    def _compute_hours(self):
        for attendance in self:
            if attendance.logout_time and attendance.login_time:
                td = attendance.logout_time - attendance.login_time
                attendance.hours = td.total_seconds() / 3600.0
            else:
                attendance.hours = 0.0

    # future date Cannot Select on DOB....
    @api.constrains('date_of_birt')
    def _check_date_end(self):
        for record in self:
            if record.date_of_birt > fields.Date.today():
                raise ValidationError("You Can Not select Future Date..")

    _sql_constraints = [
        ('name_uniq', 'UNIQUE (name)', 'Name  must be unique'),
        ('email_uniq', 'UNIQUE (email)', 'Student Email  must be unique')
    ]

    # Name  & Email  Validation (6/31)....
    # Python / Sql ... Constrain...
    # Many2many...
    # Pg Admin | Show Table/ db my own table....sql/ obj...

    # Python Constrain in name Fields..
    @api.constrains('name')
    def _check_name_data(self):
        for rec in self:
            if len(rec.name) < 5 :
                raise ValidationError(_("Name Should Not Be Less Then  5 Character "))
        return True

    def action_print(self):
        print("::::::::Button Clicked", self)
        for rec in self:
            # odoo Search Method
            student = self.env['bs.student'].search([])
            print("Student ::::::", student)
            female_student = self.env['bs.student'].search([('gender', '=', 'female')])
            print("Female_student ::::::", female_student)
            traning_student = self.env['bs.student'].search([('status', '=', 'training')])
            print("Traning_student ::::::", traning_student)


    # @api.constrains('phone')
    # def _check_phone(self):
    #     for record in self:
    #         if len(record.phone) != 10:
    #             raise ValidationError("Phone no is Not Greater then 10")





