# -*- coding: utf-8 -*-

from odoo import fields,  models, api

class BsClass(models.Model):
    """School BS Class Object"""
    _name = 'bs.class'
    _rec_name = 'name'
    # _inherit = ['rating.mixin', 'mail.thread']
    _description = "Academic Class"

    name = fields.Char(string='Name', required=True)
    subject = fields.Char(string='Subject')
    grade_id = fields.Many2one('bs.grade', string='Class', required=True) # 'bs.grade'
    employee_id = fields.Many2one('hr.employee', string='Employee ID')

    teacher_id = fields.Many2one(comodel_name='school.teacher', string="Teacher")

    progress_bar = fields.Integer(compute='_compute_progress_bar')

    @api.depends('subject', 'employee_id')
    def _compute_progress_bar(self):
        for rec in self:
            if rec.subject and rec.employee_id:
                progress = 100
            elif rec.subject:
                progress = 50
            else:
                progress = 0
            rec.progress_bar = progress