# -*- coding: utf-8 -*-

from odoo import fields,  models

class BsAcademicYear(models.Model):
    """Academic Year Object"""
    _name = 'bs.academic.year'
    _rec_name = 'financial_year'
    _description = "Academic Year Details"

    financial_year = fields.Char(string='Financial Year', required=True)
    code = fields.Char(string='Code', required=True)
    start_date = fields.Date(string='Start Date', required=True)
    end_date = fields.Date(string='End date', required=True)
    term_structure = fields.Selection([('two_semester', 'Two Semesters'),
                                       ('three_trimester', 'Three Trimester'),
                                       ('four_quarters', 'Four Quarters')], string='Term Structure')

    term_line_ids = fields.One2many('bs.academic.term', 'academic_year_id', string='Terms')

    def action_create_term(self):
        print("#####I am in the Button Call####") # pass

        return True

    # product_line_ids = fields.One2many('academic.product.line', 'product_line_id', string='Tour lines')

class ProductLine(models.Model):
    """ Tour Program Table"""
    _name = "academic.product.line"
    _description = "Academic Product Line"

    product_name_id = fields.Many2one('product.product', string='Product')
    product_price = fields.Float(related='product_name_id.list_price', string="Price")
    taxes_ids = fields.Many2many('account.tax', string='Taxes')
    taxes = fields.Float(string='TTaxes', readonly=False) # , compute='_compute_total'
    price = fields.Float(string='Price')
    total = fields.Float(string='Total', readonly=False) # , compute='_compute_total'
    product_line_id = fields.Many2one('bs.academic.year', string="Tour ID")




