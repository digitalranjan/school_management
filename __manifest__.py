############################################################################
# Bista Solutions
# Copyright (c) 2023 (http://www.bistasolutions.com)
############################################################################
{
    'name': 'School Management App',

    'depends': ['mail', 'product', 'hr', 'report_xlsx'],
    'data': [
        'security/ir.model.access.csv',
        'data/ir_sequence_data.xml',
        'views/bs_academic_year.xml',
        'views/bs_academic_term.xml',
        'views/menu_view.xml',
        'views/grade_view.xml',
        'views/class_view.xml',
        'views/family_view.xml',
        'views/student_profile_view.xml',
        'views/parent_profile_view.xml',
        'views/courses_view.xml',
        'views/subject_view.xml',
        'views/school_teacher_view.xml',
        # 'wizard/sale_order_wizard_view.xml',
        'report/report.xml',
        'report/report_view.xml'


    ],
    'demo': [],
    'installable': True,
    'auto_install': False
}